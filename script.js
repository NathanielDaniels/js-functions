//function myFirstFunction() {
//    console.log("My First Function of Many!!");
//}
//
//myFirstFunction();

//+==============================================
//Sing a Song with One Single Function
//function singSong() {
//    console.log("Twinkle Twinkle little star,");
//    console.log("How I wonder what you are");
//    console.log("Up Able the world so high");
//    console.log("like a diamond in the sky");
//}
//
//singSong();
//singSong();
//singSong();

//+==============================================

// Arguments

// Do Math
//function square(num) {
//    console.log(num * num);
//}
//
//square(10); //prints 100


// write strings
//function sayHello(name) {
//    console.log("Hello there, " + name + "!");
//}
//
//sayHello("Billy");

//---------------------

// Also Takes Multiple Functions
//function area(length, width) {
//    console.log(length * width);
//}
//
//area(9,2); //18

//---- ----------

//function greet(person1, person2, person3) {
//    console.log("hi, " + person1 + ".");
//    console.log("hello, " + person2 + ".");
//    console.log("howdy, " + person3 + ".");
//    
//}
//
//greet("Nathan", "Simone", "Peanut");

//+==============================================

// The Return Keyword

//function square(x) {
//    return (x * x);
//}
//
//console.log("5 squared is: " + square(5));
//
////----
//
//var result = square(10); //100
//
//console.log(result);

//-------------------------------------------

//This function Capitalizes the first Char in a string

//function capitalize(str) {
//    return str.charAt(0).toUpperCase() + str.slice(1);
//}
//
//var city = "paris";                //"paris"
//var capital = capitalize(city);    //"Paris"
//
//console.log(city);
//console.log(capital);

//we can capture the returned value in a variable

//-------------------------------------------

//function capitalize(str) {
//    if(typeof str === "number") {
//        return "That's Not a String!"
//    }
//    return str.charAt(0).toUpperCase() + str.slice(1);
//}
//
//var city = "paris";
//var capital = capitalize(city);
//
//var num = 37;
//var capital = capitalize(num); //"That is Not a String!
//
//console.log(capital);

//------------------------------------------

//Function Declaration vs. Function Experssion

//function declaration
function capitalize(str) {
    return str.charAt(0).toUpperCase() + str.slice(1);
}

//function expression
var capitalize = function(str) {
    return str.charAt(0).toUpperCase() + str.slice(1);
}