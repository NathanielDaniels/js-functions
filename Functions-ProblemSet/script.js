
console.log("//Return True if Even, False if Odd");

function isEven(num) {
//    "use strict";   //need this for some reason?
    if (num % 2 === 0) {
        return true;
    } else {
        return false;
    }
}

var answer = isEven(2); // true
console.log(answer);

//Shorter Version
function isEven(num) {
    return num % 2 === 0;
} 

console.log(isEven(5)); // Fale

//===============================================
console.log("//Return Factorial");

function factorial(num) {
    //define a result variable
    var result = 1;
    //calculate factorial and store value in result
    for(var i = 2; i <= num; i++) {
        result *= i;
    }
    //return the result variable
    return result;
}

console.log(factorial(5)); //120
//===============================================
//kebabToSnake()
//write a function kebabToSnake() which takes a kebab-cased string argument and returns the snake_cased version

//basically, replace "-" with "_";
console.log("//Change parts inside string ");

function kebabToSnake(str) {
    //replace all "-" with "_"'s
    var newStr = str.replace(/-/g , "_");
    //return str
    return newStr
    }
    
console.log(kebabToSnake("hello-There!")); //Hello_There!
//===============================================
